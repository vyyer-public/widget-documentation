# Postman documentation
We have a publicly available Postman collection at the following [link](https://www.postman.com/vyyerllc/workspace/vyyer-public) 
with the examples of requests for our Unified API.

# Installing Vyyer Widget
Put web component code inside HTML page:

```html
<div id="vyyer-widget-container"></div>
<script src="https://vyyer.id/pkg/latest/vyyer-widget.js" defer></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    const container = document.getElementById('vyyer-widget-container');
    const vyyerWidget = document.createElement('vyyer-widget');
    vyyerWidget.setAttribute('module-url', 'https://vyyer.id/idscan/');
    vyyerWidget.setAttribute('api-key', 'api-key');
    vyyerWidget.setAttribute('api-url', 'module-url');
    vyyerWidget.setAttribute('workflow', 'Full KYC');
    vyyerWidget.setAttribute('logo', '/logo.png');
    vyyerWidget.setAttribute('token', '');

    // In case you want to attach the validation to a user uid in your system, you can set this parameter.
    vyyerWidget.setAttribute('user-uid', 'some user id in your system or an empty value');

    // If you set this attribute, existing workflow uid is going to be used. Otherwise, the widget is going to create a new one.
    vyyerWidget.setAttribute('existing-workflow-uid', 'workflow uid of a pre-created workflow or an empty value')

    container.appendChild(vyyerWidget);

    // Optional listener for the verification state:
    window.setStateCallback((state) => {
      ...
    }
  });
</script>
```

- **module-url**: Scan module url (https://vyyer.id/idscan/)
- **api-key**: Provided by Vyyer Technologies for the domain(s) you’ve provided
- **api-url**: https://unifiedapi.vyyer.id for Cloud Unified API, hosted by Vyyer Technologies; the URL will be provided if you choose to use Self-Hosted Unified API
- **workflow**: "ID Verification" or "Full KYC"
- **logo**: A relative of absolute URL to your logo PNG or SVG file which is hosted on your domain(s)
- **user-uid**: A unique value associated with the workflow initiated for the widget
- **token**: A one-time token that can be used to authenticate the user. For example, when placing widget on a secure page that requires the user to log in, this token can be generated on your back-end, and used to authenticate the user instead of redirecting them to the login page.

- **state**: Represents the current state of scanning. Possible values: 'idle' | 'progress' | 'verdict' | 'done' | 'cancelled'. 'verdict' state is when verdict becomes available (window.vyyerVerdict or (window as any).vyyerVerdict in TypeScript). 'done' state is when user pressed "Done" button on the verdict page.

# Example

"Full KYC" includes face matching and liveness, while "ID Verification" only checks the driver's license.
In this example there are redirects to different pages if user cancels or completes the verification.

```html
<div id="vyyer-widget-container"></div>
<script src="https://vyyer.id/pkg/latest/vyyer-widget.js" defer></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    const container = document.getElementById('vyyer-widget-container');
    const vyyerWidget = document.createElement('vyyer-widget');
    vyyerWidget.setAttribute('module-url', 'https://vyyer.id/idscan/');
    vyyerWidget.setAttribute('api-key', '5cbe49ca05a9c68cbf0b1b7e89957b36');
    vyyerWidget.setAttribute('api-url', 'https://unifiedapi.vyyer.id');
    vyyerWidget.setAttribute('workflow', 'Full KYC');
    vyyerWidget.setAttribute('logo', '/logo.png');
    vyyerWidget.setAttribute('user-uid', 'random-unique-identifier-here-associated-with-session-uid-00001');
    container.appendChild(vyyerWidget);

    window.setStateCallback((state) => {
      if (state === 'cancelled') {
        window.location.href = 'some_url_which_should_be_loaded_if_scan_is_cancelled'
      } else if (state === 'done') {
        window.location.href = 'some_url_which_should_be_loaded_if_scan_is_complete'
      }
    }
  });
</script>
```

# Sample

A sample deployment is available to explore at [Web SDK](https://websdk.vyyer.id/), the latest deployment of our production release.

## Client UID
This value is supposed to be generated and inserted by your server, and serves as your unique identifier you would like to be mapped one-to-one with Vyyer' SessionUID.
For example, you can generate UUIDv4 or a unique hash for your client doing the verification, and include his/her unique identifier from your system.

# Making the request
To retrieve the identity verification data and results of a specific workflow id use the request structure below. Vyyer will provide your Vyyer Server URL, organization ID, and User ID upon request. Workflow IDs can be retrieved from the web SDK at any point during the verification process.
Note: Full Swagger documentation is given upon request.
Example Request

```shell 
curl --location 'https://[Unique Server URL Issued by Vyyer Goes Here]/api/v2/workflows/id/[Workflow ID goes here]?Children=true&APIKey=[API Key Goes Here]' \
--header 'X-User-Id: [Your User ID goes here]' \
--header 'X-Org-Id: [Your Organization ID goes here]' \
--header 'Origin: [URL of Domain using Vyyer Verification & Bound to API Key]'
```

# Response explanation
Final Verdict is at the start of the response.
```json
{
  "Errors": [],
  "SessionUID": "ae930c86-cf40-4d1a-92ac-e30baf53602a",
  "UserUID" : "random-unique-identifier-here-associated-with-session-uid",
  "Name": "Full KYC",
  "Identity": {
    "ID": 386,
    "UID": "0Wi7T/o9nMXsHK9BEApftxnbi9AyoiP6mNrhnaXn1W4=",
    "UserID": "auth0|62384ce4f0cd470068db8054",
    "OrgID": "org_M3LDoRAJvtHJMmei",
    "Orientation": 0,
    "LicenseNumber": "00000000",
    "Birthday": "90-07-07",
    "IssuedAt": "2019-10-08",
    "ExpiresAt": "2027-07-07",
    "Height": "5ft 11in",
    "Weight": "000",
    "EyeColor": "BRO",
    "HairColor": "BLK",
    "Address": "0000 Xxxxxx Xx, Richardson, TX, 750800000, USA",
    "Street": "0000 XXXXXX XX",
    "City": "RICHARDSON",
    "PostalCode": "750800000",
    "FullName": "Jason Maaraoui",
    "FirstName": "Jason",
    "MiddleName": "",
    "LastName": "Maaraoui",
    "Gender": "m",
    "Ban": 1,
    "BannedBy": "auth0|62acbf380a2a96cd305da20e",
    "BanStartAt": "2023-05-12 11:21:00",
    "BanEndAt": "1970-01-01 00:00:00",
    "BanReasonID": null,
    "VIP": 1,
    "VIPBy": "auth0|62acbf380a2a96cd305da20e",
    "VIPStartAt": "2023-05-05 16:07:38",
    "VIPEndAt": "1970-01-01 00:00:00",
    "Visits": 0,
    "State": "TX",
    "CreatedAt": "2023-03-20 15:08:50",
    "LastScannedAt": "",
    "ScansInPeriod": 0
  },
  "State": -1,
  "Verdict": 2,
  "StateName": "In Progress",
  "VerdictName": "Invalid",
  "FinalVerdictName": "Invalid",
  "Data": [
    {
      "Attempts": [
        {
          "ScanID": 3235,
          "FirstName": "Jason",
          "LastName": "Maaraoui",
          "FathersName": "",
          "MothersName": "",
          "FullName": "Jason Maaraoui",
          "Verdict": "Valid",
          "Description": "",
          "Attempt": 1
        }
      ],
      "UID": "e532e6e2-9577-48cb-ac12-19fbc49301b9",
      "Type": 2,
      "Name": "Scan Back",
      "State": "Done",
      "MaxAttempts": 5,
      "ClientCallbackURL": "",
      "ServerCallbackURL": "",
      "IFRAMEURL": "https://vyyer.id/idscan/",
      "APIURL": "APP_URL",
      "Data": [
        {
          "Key": "ScanID",
          "Value": "3235",
          "Type": "integer"
        },
        {
          "Key": "IdentityID",
          "Value": "386",
          "Type": "integer"
        },
        {
          "Key": "Verdict",
          "Value": "Valid",
          "Type": "string"
        }
      ],
      "Images": [
        {
          "Type": 2,
          "ScanID": 0,
          "Filename": "https://unifiedapi.dev.vyyer.id/storage/scans/scan_0_back_e532e6e2-9577-48cb-ac12-19fbc49301b9_2.jpg",
          "CreatedAt": "2023-05-11 17:17:14"
        }
      ],
      "ExpiresAt": "2023-05-11 17:26:00",
      "CreatedAt": "2023-05-11 17:16:30",
      "UpdatedAt": "2023-05-11 17:17:14"
    }
  ],
  "CreatedAt": "2023-05-11 17:16:30",
  "UpdatedAt": "2023-05-11 17:17:20",
  "Auth0UserID": "auth0|62acbf380a2a96cd305da20e",
  "Auth0OrgID": "org_PsOHd7fa3h6wHXlQ",
  "FirstName": "Jason",
  "MiddleName": "",
  "LastName": "Maaraoui"
}
```

One or more scan attempt follows the final result.
```json
{
  "Data": [
    {
      "Attempts": [
        {
          "ScanID": 3235,
          "FirstName": "Jason",
          "LastName": "Maaraoui",
          "FathersName": "",
          "MothersName": "",
          "FullName": "Jason Maaraoui",
          "Verdict": "Valid",
          "Description": "",
          "Attempt": 1
        }
      ],
      "UID": "e532e6e2-9577-48cb-ac12-19fbc49301b9",
      "Type": 2,
      "Name": "Scan Back",
      "State": "Done",
      "MaxAttempts": 5,
      "ClientCallbackURL": "",
      "ServerCallbackURL": "",
      "IFRAMEURL": "https://vyyer.id/idscan/",
      "APIURL": "APP_URL",
      "Data": [
        {
          "Key": "ScanID",
          "Value": "3235",
          "Type": "integer"
        },
        {
          "Key": "IdentityID",
          "Value": "386",
          "Type": "integer"
        },
        {
          "Key": "Verdict",
          "Value": "Valid",
          "Type": "string"
        }
      ],
      "Images": [
        {
          "Type": 2,
          "ScanID": 0,
          "Filename": "https://unifiedapi.dev.vyyer.id/storage/scans/scan_0_back_e532e6e2-9577-48cb-ac12-19fbc49301b9_2.jpg",
          "CreatedAt": "2023-05-11 17:17:14"
        }
      ],
      "ExpiresAt": "2023-05-11 17:26:00",
      "CreatedAt": "2023-05-11 17:16:30",
      "UpdatedAt": "2023-05-11 17:17:14"
    },
    {
      "Attempts": [
        {
          "FirstName": "JASON",
          "LastName": "MAARAOUI",
          "FathersName": "",
          "MothersName": "",
          "FullName": "JASON MAARAOUI",
          "DateOfBirth": "1990-07-09",
          "DateOfIssue": "2019-10-10",
          "DateOfExpiry": "2027-07-09",
          "Verdict": "Valid",
          "Description": "",
          "Attempt": 1
        }
      ],
      "UID": "4eb3c75f-409e-4eed-afcb-4af259c45444",
      "Type": 1,
      "Name": "Scan Front",
      "State": "Done",
      "MaxAttempts": 5,
      "ClientCallbackURL": "",
      "ServerCallbackURL": "",
      "IFRAMEURL": "https://vyyer.id/idscan/",
      "APIURL": "APP_URL",
      "Data": [
        {
          "Key": "Verdict",
          "Value": "Valid",
          "Type": "string"
        }
      ],
      "Images": [
        {
          "Type": 5,
          "ScanID": 0,
          "Filename": "https://unifiedapi.dev.vyyer.id/storage/faces/scan_0_face_4eb3c75f-409e-4eed-afcb-4af259c45444_1.jpg",
          "CreatedAt": "2023-05-11 17:16:34"
        },
        {
          "Type": 1,
          "ScanID": 0,
          "Filename": "https://unifiedapi.dev.vyyer.id/storage/scans/scan_0_front_4eb3c75f-409e-4eed-afcb-4af259c45444_1.jpg",
          "CreatedAt": "2023-05-11 17:16:34"
        }
      ],
      "ExpiresAt": "2023-05-11 17:26:00",
      "CreatedAt": "2023-05-11 17:16:30",
      "UpdatedAt": "2023-05-11 17:17:14"
    },
    {
      "Attempts": [
        {
          "Percent": 0,
          "Verdict": "Invalid",
          "Description": "",
          "Attempt": 1
        }
      ],
      "UID": "952a01e0-54ec-4d26-92de-3f3dc890990a",
      "Type": 4,
      "Name": "Face Match",
      "State": "Registered",
      "MaxAttempts": 5,
      "ClientCallbackURL": "",
      "ServerCallbackURL": "",
      "IFRAMEURL": "",
      "APIURL": "APP_URL",
      "Data": [
        {
          "Key": "Percent",
          "Value": "0",
          "Type": "integer"
        },
        {
          "Key": "Verdict",
          "Value": "Invalid",
          "Type": "string"
        }
      ],
      "Images": [
        {
          "Type": 6,
          "ScanID": 0,
          "Filename": "https://unifiedapi.dev.vyyer.id/storage/selfies/selfie_952a01e0-54ec-4d26-92de-3f3dc890990a_1.jpg",
          "CreatedAt": "2023-05-11 17:16:31"
        }
      ],
      "ExpiresAt": "2023-05-11 17:26:00",
      "CreatedAt": "2023-05-11 17:16:30",
      "UpdatedAt": "2023-05-11 17:17:20"
    }
  ],
  "CreatedAt": "2023-05-11 17:16:30",
  "UpdatedAt": "2023-05-11 17:17:20",
  "Auth0UserID": "auth0|62acbf380a2a96cd305da20e",
  "Auth0OrgID": "org_PsOHd7fa3h6wHXlQ",
  "FirstName": "Jason",
  "MiddleName": "",
  "LastName": "Maaraoui"
}
```